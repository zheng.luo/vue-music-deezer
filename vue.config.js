/* eslint import/no-extraneous-dependencies: 0 */

const webpack = require('webpack');

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    output: {
      publicPath: '/',
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.DefinePlugin({
        BASE_PATH: JSON.stringify('/'),
      }),
    ],
  },
};
