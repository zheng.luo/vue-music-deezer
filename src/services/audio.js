import store from '../store/index';

let audio;
let currentVol = 0.7;
let lastTrackId;

const isAudioReady = () => audio && audio.readyState !== 0;

const getTime = (event) => {
  const { currentTime, duration } = event.target;
  return {
    currentTime,
    duration,
    progress: currentTime / duration * 100 || 0,
  };
};

const loadStartListener = () => store.dispatch('setAudioLoading', true);

const canplayListener = () => store.dispatch('setAudioLoading', false);

const endedListener = () => store.dispatch('audioEnd');

const timeupdateListener = (event) => store.dispatch('audioUpdate', getTime(event));

const errorListener = (error) => {
  console.error('Error on playing audio', error);
  store.dispatch('audioError', error);
};

const setAudioListeners = () => {
  if (!audio) {
    return;
  }
  audio.addEventListener('loadstart', loadStartListener);
  audio.addEventListener('canplay', canplayListener);
  audio.addEventListener('ended', endedListener);
  audio.addEventListener('timeupdate', timeupdateListener);
  audio.addEventListener('error', errorListener);
};

export function removeAudioListeners() {
  if (!audio) {
    return;
  }
  audio.removeEventListener('loadstart', loadStartListener);
  audio.removeEventListener('canplay', canplayListener);
  audio.removeEventListener('ended', endedListener);
  audio.removeEventListener('timeupdate', timeupdateListener);
  audio.removeEventListener('error', errorListener);
}

export const play = track => {
  if (audio) {
    audio.pause();
  }
  const resumeOldTrack = lastTrackId && track.id === lastTrackId;
  if (resumeOldTrack) {
    audio.play();
    return;
  }
  audio = new Audio();
  audio.src = track.preview;
  audio.volume = currentVol;
  const promise = audio.play();
  if (promise && promise.catch) {
    promise.catch(error => console.log(error));
  }
  lastTrackId = track.id;
  if (!resumeOldTrack) {
    setAudioListeners();
  }
};

export const pause = () => {
  if (audio) {
    audio.pause();
  }
};

export const setVolume = volume => {
  if (!isAudioReady()) {
    return;
  }
  currentVol = volume;
  audio.volume = volume;
};

export const seek = progress => {
  if (!isAudioReady()) {
    return;
  }
  audio.currentTime = audio.duration * progress / 100;
};

export function destroy() {
  if (audio) {
    audio.pause();
  }
  removeAudioListeners();
  audio = null;
}
