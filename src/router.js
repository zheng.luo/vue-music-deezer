import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home';
import Play from './pages/Play';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/play/:type?/:id?/:trackId?',
      name: 'play',
      component: Play,
    },
  ],
  mode: 'history',
  scrollBehavior(to, from) {
    if (to.name === 'play' && from.name === 'play') {
      return;
    }
    return { x: 0, y: 0 };
  },
  base: BASE_PATH || '/',
});
