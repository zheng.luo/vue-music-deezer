import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import home from './home';
import search from './search';
import play from './play';
import audio from './audio';
import sidebar from './sidebar';
import genres from './genres';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  modules: {
    home,
    search,
    play,
    audio,
    sidebar,
    genres,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});

export default store;
