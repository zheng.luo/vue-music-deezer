import Vue from 'vue';

export const eventBus = new Vue();

export const removeDuplicates = (array, property) => {
  const uniq = new Set();
  return array.filter(item => {
    const isUniq = !uniq.has(item[property]);
    uniq.add(item[property]);
    return isUniq;
  });
};

export const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

export function serializeGetParams(params) {
  return Object.keys(params).reduce((accu, key) => {
    if (accu !== '') {
      return `${accu}&${key}=${encodeURIComponent(params[key])}`;
    }
    return `${accu}${key}=${encodeURIComponent(params[key])}`;
  }, '');
}
