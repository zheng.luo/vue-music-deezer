import moment from 'moment';

const REPLACER_PATTERN = /(.)(?=(\d{3})+$)/g;

export const playsCount = value => {
  if (value < 1000) return value;
  return String(value).replace(REPLACER_PATTERN, '$1,');
};

export const playsCountShort = value => {
  if (value < 1000) return value;
  const str = String(value);
  if (value < 1000000) {
    return `${str.slice(0, str.length - 3)}K`;
  }
  return `${str.slice(0, str.length - 6)}M`;
};

export const trackTime = (time, unit) => {
  if (typeof time !== 'number' || !time) {
    return '0:00';
  }
  if (unit === 'ms') {
    time /= 1000;
  }
  const hours = Math.floor(time / 3600);
  const minutes = `${Math.floor((time % 3600) / 60)}`.slice(-2);
  const seconds = `0${Math.floor(time % 60)}`.slice(-2);
  if (hours) {
    return `${hours}:${minutes}:${seconds}`;
  }
  return `${minutes}:${seconds}`;
};

export const timePassed = date => moment(date).format('d MMMM Y');
