import 'vue-material/dist/vue-material.min.css';
import './assets/styles.styl';
import 'babel-polyfill';
import Vue from 'vue';
import { MdButton, MdTooltip, MdIcon, MdSnackbar, MdMenu, MdList } from 'vue-material/dist/components';
import App from './App.vue';
import router from './router';
import store from './store/index';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Sidebar from './components/sidebar/Sidebar';
import Player from './components/player/Player';
import PlayCard from './components/tiles/PlayCard';
import ChartTile from './components/tiles/ChartTile';
import TrackRow from './components/track/TrackRow';
import ActiveTrack from './components/track/ActiveTrack';
import AnimatedPlaceholder from './components/misc/AnimatedPlaceholder';
import Snackbar from './components/misc/Snackbar';
import Spinner from './components/misc/Spinner';
import { trackTime, playsCount, playsCountShort, timePassed } from './filters';

Vue.config.productionTip = false;

Vue.use(MdButton);
Vue.use(MdTooltip);
Vue.use(MdIcon);
Vue.use(MdSnackbar);
Vue.use(MdMenu);
Vue.use(MdList);

Vue.component('main-header', Header);
Vue.component('main-footer', Footer);
Vue.component('sidebar', Sidebar);
Vue.component('bottom-player', Player);
Vue.component('play-card', PlayCard);
Vue.component('chart-tile', ChartTile);
Vue.component('track-row', TrackRow);
Vue.component('active-track', ActiveTrack);

Vue.component('animated-placeholder', AnimatedPlaceholder);
Vue.component('main-snackbar', Snackbar);
Vue.component('spinner', Spinner);

Vue.filter('trackTime', trackTime);
Vue.filter('playsCount', playsCount);
Vue.filter('playsCountShort', playsCountShort);
Vue.filter('timePassed', timePassed);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
